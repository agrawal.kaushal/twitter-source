package com.rightdata.twitterapi.Controller;

import com.rightdata.twitterapi.Model.ResponseDTO;
import com.rightdata.twitterapi.Model.Rules;
import com.rightdata.twitterapi.Model.TwitterRequestDTO;
import com.rightdata.twitterapi.Service.TwitterFilteredStreamService;
import com.rightdata.twitterapi.Service.TwitterStreamService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;
import org.stringtemplate.v4.ST;
import twitter4j.TwitterException;
import twitter4j.TwitterStream;

import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.util.*;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping(value = "/twitter")
public class TwitterApiController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    TwitterStreamService twitterStreamService;
    @Autowired
    TwitterFilteredStreamService twitterFilteredStreamService;

    private Future<String> numberListTask;

    List<TwitterStream> twitterStreams = new ArrayList<>();
    List<HttpURLConnection> threads = new ArrayList<>();
    public static Map<String, HttpURLConnection> connectionMap = new HashMap<>();
    private StreamingResponseBody stream;

    @RequestMapping(method = RequestMethod.POST, value = "/stream2")
    public ResponseEntity<ResponseDTO> streamTwitterApi(@RequestBody TwitterRequestDTO requestDTO) throws IOException, URISyntaxException {
        ResponseDTO responseDTO = new ResponseDTO();
        TwitterStream twitterStream = twitterStreamService.startStreamingTweets(requestDTO);
        twitterStreams.add(twitterStream);
        try {
            responseDTO.setMessage(String.valueOf(twitterStream.getId()));
            responseDTO.setStatus(true);
            return new ResponseEntity<ResponseDTO>(responseDTO, HttpStatus.OK);
        } catch (TwitterException e) {
            e.printStackTrace();
            responseDTO.setStatus(false);
            return new ResponseEntity<ResponseDTO>(responseDTO, HttpStatus.OK);
        }

    }

    @RequestMapping(value = "/stopJob2/{index}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDTO> cancel(@PathVariable("index") long index) {
        ResponseDTO responseDTO = new ResponseDTO();
        List<TwitterStream> activeStreams = twitterStreams.stream()
                .filter(stream -> {
                    try {
                        return stream.getId() == index;
                    } catch (TwitterException e) {
                        e.printStackTrace();
                    }
                    return false;
                })
                .collect(Collectors.toList());
        for (TwitterStream stream: activeStreams) {
            stream.clearListeners();
            stream.cleanUp();
            stream.shutdown();
        }
        responseDTO.setMessage("Stream Stopped");
        responseDTO.setStatus(true);
        return new ResponseEntity<ResponseDTO>(responseDTO, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/stream")
    public ResponseEntity<String> streamTwitterApi2(@RequestBody TwitterRequestDTO requestDTO) {
        ResponseDTO responseDTO = new ResponseDTO();
        String streamId = requestDTO.getStreamId();

        if (requestDTO.getTwitterConnection().getVersion().equals("2")) {
            boolean rulesPresent = false;
            Rules[] rules = (requestDTO.getTwitterConnection().getRules() != null) ? requestDTO.getTwitterConnection().getRules() : new Rules[0];
            for (Rules rule: rules) {
                if (rule.getValue() != null && !rule.getValue().isEmpty() &&
                        rule.getTag() != null && !rule.getTag().isEmpty()) {
                    rulesPresent = true;
                }
            }
            if (!rulesPresent) {
                responseDTO.setConnection(null);
                responseDTO.setStatus(false);
                responseDTO.setMessage("Rules not found");
                return new ResponseEntity<String>(responseDTO.toString(), HttpStatus.CONFLICT);
            }
        }

        try {
            responseDTO = twitterFilteredStreamService.connectStream(requestDTO);
            HttpURLConnection connection = responseDTO.getConnection();
            for (Map.Entry<String, HttpURLConnection> entry : connectionMap.entrySet()) {
                String key = entry.getKey();
                HttpURLConnection value = entry.getValue();
                if (key.equals(streamId)) {
                    value.disconnect();
                    connectionMap.remove(key);
                }
            }
            connectionMap.put(streamId, connection);
            responseDTO.setConnection(null);
            return new ResponseEntity<String>(responseDTO.toString(), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            responseDTO.setStatus(false);
            responseDTO.setMessage(e.getMessage());
            responseDTO.setError(e.getLocalizedMessage());
            return new ResponseEntity<String>(responseDTO.toString(), HttpStatus.CONFLICT);
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/stopJob/{streamId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> cancel2(@PathVariable("streamId") String streamId) throws IOException {
        ResponseDTO responseDTO = new ResponseDTO();
        boolean flag = false;
        for (Map.Entry<String, HttpURLConnection> entry : connectionMap.entrySet()) {
            String key = entry.getKey();
            HttpURLConnection value = entry.getValue();
            if (key.equals(streamId)) {
                value.disconnect();
                connectionMap.remove(key);
                responseDTO.setMessage("Stream Stopped");
                responseDTO.setStatus(true);
                return new ResponseEntity<String>(responseDTO.toString(), HttpStatus.OK);
            }
        }
        responseDTO.setMessage("Stream not found");
        responseDTO.setStatus(false);
        return new ResponseEntity<String>(responseDTO.toString(), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/get-sample-record")
    public ResponseEntity<String> streamTwitterApi2Preview(@RequestBody TwitterRequestDTO requestDTO) {
//        ResponseDTO responseDTO = new ResponseDTO();
//        String streamId = requestDTO.getStreamId();
        try {
            String response = twitterFilteredStreamService.sampleRun(requestDTO);
//            responseDTO.getConnection().disconnect();
//            responseDTO.setConnection(null);
            return new ResponseEntity<String>(response, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
//            responseDTO.setStatus(false);
//            responseDTO.setMessage(e.getMessage());
//            responseDTO.setError(e.getLocalizedMessage());
            return new ResponseEntity<String>(e.getLocalizedMessage(), HttpStatus.CONFLICT);
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/stopAll", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> cancelAll() throws IOException {
        ResponseDTO responseDTO = new ResponseDTO();

        for (Iterator<Map.Entry<String, HttpURLConnection>>  entry = TwitterApiController.connectionMap.entrySet().iterator(); entry.hasNext();) {
            Map.Entry<String, HttpURLConnection> entry1 = entry.next();
            HttpURLConnection value = entry1.getValue();
            value.disconnect();
            entry.remove();
        }

        connectionMap.clear();
        responseDTO.setMessage("All Streams Stopped" + connectionMap.size());
        responseDTO.setStatus(true);

        return new ResponseEntity<String>(responseDTO.toString(), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/get-fields", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getFields() throws IOException {
        ResponseDTO responseDTO = new ResponseDTO();

        String fields = twitterFilteredStreamService.getFields();
        responseDTO.setMessage(fields);
        responseDTO.setStatus(true);

        return new ResponseEntity<String>(responseDTO.toString(), HttpStatus.OK);
    }


    @RequestMapping(method = RequestMethod.POST, value = "/stream-query", produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
    public ResponseEntity<StreamingResponseBody> processSQL(final HttpServletResponse response) {
        String json = null;
//        StreamingResponseBody stream = queryService.streamResponse(queryServiceString);

//        ResponseEntity<ResponsePOJO> res = queryService.processQuery(queryServiceString);

        response.setContentType("application/stream+json");
        StreamingResponseBody stream = out -> {
            try {
////            try(ObjectOutputStream oos = new ObjectOutputStream(out)) {
                int i = 1;
//                Thread loop = new Thread(
//                        new Runnable() {
//                            @Override
//                            public void run() {
//                                while (true) {
//                                    if (Thread.interrupted()) {
//                                        break;
//                                    }
////                                    while (true) {
//                                        String content = "{\"counter\":" + i+1 + "}\n";
//                                        try {
//                                            out.write(content.getBytes());
//                                            out.flush();
//                                            Thread.sleep(500);
//                                        } catch (IOException | InterruptedException e) {
//                                            e.printStackTrace();
//                                        }
////                                    }
//                                }
//                            }
//                        }
//                );
//                loop.start();

                while (true) {
                    String content = "{\"counter\":" + i++ + "}\n";
                    try {
                        out.write(content.getBytes());
                        out.flush();
                        Thread.sleep(500);
                    } catch (IOException | InterruptedException e) {
                        e.printStackTrace();
                    }
                }
//                for (int i = 0; i < 10; i++) {
////                        oos.write(queryServiceString.toString().getBytes());
//                        String content = "{\"counter\":" + i + "}\n";
//                    out.write(content.getBytes());
//                    out.write(queryServiceString.toString().getBytes());
////                        oos.flush();
//                        out.flush();
////                        logger.info("size: " + content.getBytes().length);
//                        Thread.sleep(3000);
//
//                }
//                out.close();
            } catch (final Exception e) {
                e.printStackTrace();
                logger.error("Exception", e);
            }
        };
        logger.info("steaming response {} ", stream);
        return new ResponseEntity(stream, HttpStatus.OK);
    }

    @GetMapping(value = "/consume", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> consume(@RequestBody TwitterRequestDTO requestDTO) throws IOException {
        URL url = new URL("http://localhost:8088/twitter/stream-query");
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("POST");
        connection.setRequestProperty ("Authorization", String.format("Bearer %s", requestDTO.getToken()));
        connection.setDoOutput(true);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(connection.getInputStream())
        );

        String decodedString;
        while ((decodedString = in.readLine()) != null) {
            System.out.println(decodedString);
            logger.info(decodedString);
        }

        in.close();

        return new ResponseEntity("ok", HttpStatus.OK);
    }

    @GetMapping(value = "/push", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> push() {
        this.stream = out -> {
            try {
////            try(ObjectOutputStream oos = new ObjectOutputStream(out)) {
//                for (int i = 0; i < 10; i++) {
////                        oos.write(queryServiceString.toString().getBytes());
                        String content = "{\"counter\":" + 1 + "}\n";
                    out.write(content.getBytes());
////                    out.write(queryServiceString.toString().getBytes());
////                        oos.flush();
                        out.flush();
////                        logger.info("size: " + content.getBytes().length);
//                        Thread.sleep(1000);
//
//                }
//                out.close();
            } catch (final Exception e) {
                logger.error("Exception", e);
            }
        };
        return new ResponseEntity("ok", HttpStatus.OK);
    }
}
