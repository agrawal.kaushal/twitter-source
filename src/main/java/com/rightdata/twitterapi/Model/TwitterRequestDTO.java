package com.rightdata.twitterapi.Model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

public class TwitterRequestDTO implements Serializable {
    private String streamId;
    private String streamMetaName;
    private String loginUserName;
    private String description;
    private long userId;
    private long clientId;
    private long folderId;
    private String name;
    private String token;
    private String stepId;

    private TwitterConnection twitterConnection;
    private SparkRuntimeProperties sparkRuntimeProperties;
    private List<StepsInfo> stepsInfo;

    public String getStreamId() {
        return streamId;
    }

    public void setStreamId(String streamId) {
        this.streamId = streamId;
    }

    public String getStreamMetaName() {
        return streamMetaName;
    }

    public void setStreamMetaName(String streamMetaName) {
        this.streamMetaName = streamMetaName;
    }

    public String getLoginUserName() {
        return loginUserName;
    }

    public void setLoginUserName(String loginUserName) {
        this.loginUserName = loginUserName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getClientId() {
        return clientId;
    }

    public void setClientId(long clientId) {
        this.clientId = clientId;
    }

    public long getFolderId() {
        return folderId;
    }

    public void setFolderId(long folderId) {
        this.folderId = folderId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TwitterConnection getTwitterConnection() {
        return twitterConnection;
    }

    public void setTwitterConnection(TwitterConnection twitterConnection) {
        this.twitterConnection = twitterConnection;
    }

    public SparkRuntimeProperties getSparkRuntimeProperties() {
        return sparkRuntimeProperties;
    }

    public void setSparkRuntimeProperties(SparkRuntimeProperties sparkRuntimeProperties) {
        this.sparkRuntimeProperties = sparkRuntimeProperties;
    }

    public List<StepsInfo> getStepsInfo() {
        return stepsInfo;
    }

    public void setStepsInfo(List<StepsInfo> stepsInfo) {
        this.stepsInfo = stepsInfo;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getStepId() {
        return stepId;
    }

    public void setStepId(String stepId) {
        this.stepId = stepId;
    }
}

class SparkRuntimeProperties implements Serializable {
    private String executorMemory;
    private String driverMemory;
    private String master;
    private String applicationName;
    private String shuffleService;
    private String sqlPartitionShuffle;
    private String maxCores;
    private String jars;
    private String dynamicAllocation;

    public String getExecutorMemory() {
        return executorMemory;
    }

    public void setExecutorMemory(String executorMemory) {
        this.executorMemory = executorMemory;
    }

    public String getDriverMemory() {
        return driverMemory;
    }

    public void setDriverMemory(String driverMemory) {
        this.driverMemory = driverMemory;
    }

    public String getMaster() {
        return master;
    }

    public void setMaster(String master) {
        this.master = master;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public String getShuffleService() {
        return shuffleService;
    }

    public void setShuffleService(String shuffleService) {
        this.shuffleService = shuffleService;
    }

    public String getSqlPartitionShuffle() {
        return sqlPartitionShuffle;
    }

    public void setSqlPartitionShuffle(String sqlPartitionShuffle) {
        this.sqlPartitionShuffle = sqlPartitionShuffle;
    }

    public String getMaxCores() {
        return maxCores;
    }

    public void setMaxCores(String maxCores) {
        this.maxCores = maxCores;
    }

    public String getJars() {
        return jars;
    }

    public void setJars(String jars) {
        this.jars = jars;
    }

    public String getDynamicAllocation() {
        return dynamicAllocation;
    }

    public void setDynamicAllocation(String dynamicAllocation) {
        this.dynamicAllocation = dynamicAllocation;
    }
}

class StepsInfo implements Serializable {
    private String stepType;
    private long stepId;
    private String sourceType;
    private String eventType;
    private List<InFlowIdList> inFlowIdList;
    private List<OutFlowIdList> outFlowIdList;
    private ConnectionInfo connectionInfo;
    private SourceParameters sourceParameters;

    public String getStepType() {
        return stepType;
    }

    public void setStepType(String stepType) {
        this.stepType = stepType;
    }

    public long getStepId() {
        return stepId;
    }

    public void setStepId(long stepId) {
        this.stepId = stepId;
    }

    public String getSourceType() {
        return sourceType;
    }

    public void setSourceType(String sourceType) {
        this.sourceType = sourceType;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public List<InFlowIdList> getInFlowIdList() {
        return inFlowIdList;
    }

    public void setInFlowIdList(List<InFlowIdList> inFlowIdList) {
        this.inFlowIdList = inFlowIdList;
    }

    public List<OutFlowIdList> getOutFlowIdList() {
        return outFlowIdList;
    }

    public void setOutFlowIdList(List<OutFlowIdList> outFlowIdList) {
        this.outFlowIdList = outFlowIdList;
    }

    public ConnectionInfo getConnectionInfo() {
        return connectionInfo;
    }

    public void setConnectionInfo(ConnectionInfo connectionInfo) {
        this.connectionInfo = connectionInfo;
    }

    public SourceParameters getSourceParameters() {
        return sourceParameters;
    }

    public void setSourceParameters(SourceParameters sourceParameters) {
        this.sourceParameters = sourceParameters;
    }
}

class InFlowIdList implements Serializable {

}

class OutFlowIdList implements Serializable {

}


class ConnectionInfo implements Serializable {
    private boolean existingConnection;
    private String connectionJDBCURL;
    private String username;
    private String password;
    private String className;
    private String schema;
    private String catalog;
    private long connectionId;

    public boolean isExistingConnection() {
        return existingConnection;
    }

    public void setExistingConnection(boolean existingConnection) {
        this.existingConnection = existingConnection;
    }

    public String getConnectionJDBCURL() {
        return connectionJDBCURL;
    }

    public void setConnectionJDBCURL(String connectionJDBCURL) {
        this.connectionJDBCURL = connectionJDBCURL;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getSchema() {
        return schema;
    }

    public void setSchema(String schema) {
        this.schema = schema;
    }

    public String getCatalog() {
        return catalog;
    }

    public void setCatalog(String catalog) {
        this.catalog = catalog;
    }

    public long getConnectionId() {
        return connectionId;
    }

    public void setConnectionId(long connectionId) {
        this.connectionId = connectionId;
    }
}


class SourceParameters implements Serializable {
    private String tempTablename;
    private String consumerKey;
    private String consumerSecret;
    private String accessToken;
    private String accessTokenSecret;

    public String getTempTablename() {
        return tempTablename;
    }

    public void setTempTablename(String tempTablename) {
        this.tempTablename = tempTablename;
    }

    public String getConsumerKey() {
        return consumerKey;
    }

    public void setConsumerKey(String consumerKey) {
        this.consumerKey = consumerKey;
    }

    public String getConsumerSecret() {
        return consumerSecret;
    }

    public void setConsumerSecret(String consumerSecret) {
        this.consumerSecret = consumerSecret;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getAccessTokenSecret() {
        return accessTokenSecret;
    }

    public void setAccessTokenSecret(String accessTokenSecret) {
        this.accessTokenSecret = accessTokenSecret;
    }
}