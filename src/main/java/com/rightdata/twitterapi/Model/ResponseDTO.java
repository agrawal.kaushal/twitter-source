package com.rightdata.twitterapi.Model;

import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import java.io.Serializable;
import java.net.HttpURLConnection;

public class ResponseDTO implements Serializable {

    boolean status;
    String message;
    String error;
    String folderPath;
    String streamId;
    String submitType;
    String fileType;
    HttpURLConnection connection;
    String topicId;

    public ResponseDTO(boolean status, String message) {
        this.status = status;
        this.message = message;
    }

    public ResponseDTO() {
    }


    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getFolderPath() {
        return folderPath;
    }

    public void setFolderPath(String folderPath) {
        this.folderPath = folderPath;
    }

    public String getStreamId() {
        return streamId;
    }

    public void setStreamId(String streamId) {
        this.streamId = streamId;
    }

    public HttpURLConnection getConnection() {
        return connection;
    }

    public void setConnection(HttpURLConnection connection) {
        this.connection = connection;
    }

    public String getSubmitType() {
        return submitType;
    }

    public void setSubmitType(String submitType) {
        this.submitType = submitType;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getTopicId() {
        return topicId;
    }

    public void setTopicId(String topicId) {
        this.topicId = topicId;
    }

    @Override
    public String toString() {
        return "{" +
                "\"status\":" + status +
                ", \"message\":\"" + message + "\"" +
                ", \"error\":\"" + error + "\"" +
                ", \"folderPath\":\"" + folderPath + "\"" +
                ", \"streamId\":\"" + streamId + "\"" +
                ", \"submitType\":\"" + submitType + "\"" +
                ", \"fileType\":\"" + fileType + "\"" +
                ", \"topicId\":\"" + topicId + "\"" +
                "}";
    }

}
