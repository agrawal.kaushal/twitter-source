package com.rightdata.twitterapi.Service;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.rightdata.twitterapi.Controller.TwitterApiController;
import com.rightdata.twitterapi.Model.ResponseDTO;
import com.rightdata.twitterapi.Model.Rules;
import com.rightdata.twitterapi.Model.TwitterRequestDTO;
import com.rightdata.twitterapi.Utils.JwtTokenUtil;
import com.rightdata.twitterapi.Utils.SparkSessionUtil;
import com.rightdata.twitterapi.Utils.TwitterAPIUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

@Service
public class TwitterFilteredStreamService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private static FileWriter file;
    private String tempFileLocation = "C:/twitter_data";
//    @Value("${oauth.consumerKey}")
    private String consumerKey;
//    @Value("${oauth.consumerSecret}")
    private String consumerSecret;
//    @Value("${oauth.accessToken}")
    private String accessToken;
//    @Value("${oauth.accessTokenSecret}")
    private String accessTokenSecret;
    private boolean isV2;
    @Value("${s3.accessKey}")
    private String accessKey;
    @Value("${s3.secretKey}")
    private String secretKey;
    @Value("${s3.region}")
    private String region;
    @Value("${s3.bucket}")
    private String bucketName;
    /*
     * This method calls the filtered stream endpoint and streams Tweets from it
     * */
    public ResponseDTO connectStream(TwitterRequestDTO requestDTO) throws IOException, URISyntaxException, Exception {

        consumerKey = requestDTO.getTwitterConnection().getConsumerToken();
        consumerSecret = requestDTO.getTwitterConnection().getConsumerSecret();
        accessToken = requestDTO.getTwitterConnection().getAccessToken();
        accessTokenSecret = requestDTO.getTwitterConnection().getAccessSecret();
        isV2 = requestDTO.getTwitterConnection().getVersion().equals("2");
//        if (requestDTO.getTwitterConnection().getType().equals("stream")) {
            return runStream(requestDTO);
//        } else {
//            return runBatch(requestDTO);
//        }

//        HttpResponse response = httpClient.execute(httpGet);
//        HttpEntity entity = response.getEntity();

    }

    @Async
    public ResponseDTO runStream(TwitterRequestDTO requestDTO) throws IOException, URISyntaxException {
        ResponseDTO responseDTO = new ResponseDTO();
        String urlString = requestDTO.getTwitterConnection().getUrl().replaceAll(" ","");
        String token = requestDTO.getTwitterConnection().getToken();
        String streamId = requestDTO.getStreamId();
        String stepId = requestDTO.getStepId();
        List<String> metadata = getMetadata(requestDTO.getTwitterConnection().getSourceSchema());

        if (isV2) {
            setupRules(token, requestDTO);
        }

        URL url = new URL(urlString);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");

        String authorizationHeader = getAuthorizationToken(requestDTO);

        if (isV2) {
            connection.setRequestProperty ("Authorization", String.format("Bearer %s", token));
        } else {
            connection.setRequestProperty("Authorization", authorizationHeader);
        }
//        connection.setDoOutput(true);
//        connection.setRequestProperty("content-type", "application/json");
//
//        String urlParameters  = "track=modi";
//        byte[] postData       = urlParameters.getBytes( StandardCharsets.UTF_8 );
//        int    postDataLength = postData.length;
//        connection.setRequestProperty( "Content-Length", Integer.toString( postDataLength ));
//        try( DataOutputStream wr = new DataOutputStream( connection.getOutputStream())) {
//            wr.write( postData );
//        }

        String streamPath = createDirectory(String.valueOf(requestDTO.getStreamId()), "twitter_stream");
//
        responseDTO.setStatus(true);
        responseDTO.setMessage("Stream Started");
        responseDTO.setConnection(connection);
        responseDTO.setStreamId(requestDTO.getStreamId());
        responseDTO.setFileType("json");
        responseDTO.setSubmitType("s3");
        String[] path = streamPath.split("twitter_data");
        responseDTO.setFolderPath("s3a://" + bucketName + "/twitter_data" + path[path.length - 1].replaceAll("\\\\", "/"));
        InputStream inputStream = null;
        inputStream = connection.getInputStream();
        InputStreamReader inputStreamReader = null;
        inputStreamReader = new InputStreamReader(inputStream);
        BufferedReader reader = null;
        reader = new BufferedReader(inputStreamReader);
        BufferedReader finalReader = reader;
        InputStreamReader finalInputStreamReader = inputStreamReader;
        InputStream finalInputStream = inputStream;

        initializeS3Directory(bucketName, streamPath);
        String topicId = requestDTO.getStreamId() + "_" + requestDTO.getStepId() + "_" + UUID.randomUUID().toString() + "_source";
        responseDTO.setTopicId(topicId);
//        List<String> metadata = getMetadata("created_at:string|id:string|text:string");
        Thread errThread = new Thread(() -> {
            try {
                int i = 0;
                long startTime = new Date().getTime()/ 1000;
                long timeInterval = requestDTO.getTwitterConnection().getTimeInterval();
                long maxBatchSize = requestDTO.getTwitterConnection().getMaxBatchSize();
                String topicName = topicId;
                String stepId1 = stepId;
                String streamId1 = streamId;
//                long batchSize = requestDTO.getTwitterConnection().getBatchSize() != 0 ? requestDTO.getTwitterConnection().getBatchSize() : maxBatchSize;
//                boolean isBatchMode = requestDTO.getTwitterConnection().isBatchMode();

                List<JSONObject> tweetsList = new ArrayList<>();
                int count = 0;

                String status = "";
                while (status != null) {
                    status = finalReader.readLine();
                    try {
                        System.out.println(status);
                        JSONObject tweet = new JSONObject(status);
                        if (isV2) {
                            tweet = (JSONObject) tweet.get("data");
                        } else {
                            JSONObject isDelete = null;
                            try {
                                tweet.get("delete");
                                tweet = null;
                            } catch (JSONException e) {}
                        }
                        if (tweet!=null) {
                            JSONObject tweet2 = new JSONObject();
                            System.out.println(tweet);
                            for (String field: metadata) {
                                if (tweet.has(field)) {
                                    tweet2.put(field, tweet.get(field));
                                }
                            }
                            System.out.println(tweet2);
                            tweetsList.add(tweet2);
                            i = i + 1;
                        }
                        long currentTime = new Date().getTime() / 1000;
                        if (currentTime - startTime > timeInterval || i % maxBatchSize == 0) {
                            System.out.println("inside time interval " + i + " - " + (currentTime - startTime > timeInterval));
                            System.out.println("inside batch " + i + " - " + (i % maxBatchSize == 0));
                            int length = tweetsList.size();
//                            List<JSONObject> subQueue = tweetsList.subList(0, length);
                            if (length > 0) {
                                System.out.println("inside s3 " + length);
                                saveTweetsToJSONFile(tweetsList, streamPath);
                                count += length;
//                                KafkaProducerService.sendMessage(topicName, stepId1, streamId1, String.valueOf(count));
                            }
                            tweetsList.clear();
                            currentTime = new Date().getTime() / 1000;
                            startTime = new Date().getTime() / 1000;
                        }
                    } catch (Exception e){
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                System.out.println(e);
                e.printStackTrace();
            } finally {
                try {
                    finalInputStreamReader.close();
                    Objects.requireNonNull(finalInputStream).close();
                    finalReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        errThread.start();
        return responseDTO;
    }

    private List<String> getMetadata(String schemaString) throws IOException, URISyntaxException {
        String[] schemas = schemaString.split("\\|");
        List<String> fields = new ArrayList<>();
        for (String schema: schemas) {
            fields.add(schema.split(":")[0]);
        }
        return fields;
    }

    private void initializeS3Directory(String bucketName, String path) {
        String[] dirPath = path.split("twitter_data");
        path = "twitter_data" + dirPath[dirPath.length - 1] + "/";
        AmazonS3 s3 = connectToS3();
        path = path.replaceAll("\\\\", "/");
        ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentLength(0);
        InputStream emptyContent = new ByteArrayInputStream(new byte[0]);
        s3.putObject(bucketName, path, emptyContent, metadata);
    }

    private String getAuthorizationToken(TwitterRequestDTO requestDTO) {
        if (!isV2) {
            // generate authorization header
            String urlString = requestDTO.getTwitterConnection().getUrl();
            String get_or_post = "GET";
            String oauth_signature_method = "HMAC-SHA1";

            String uuid_string = UUID.randomUUID().toString();
            uuid_string = uuid_string.replaceAll("-", "");
            String oauth_nonce = uuid_string; // any relatively random alphanumeric string will work here

            // get the timestamp
            Calendar tempcal = Calendar.getInstance();
            long ts = tempcal.getTimeInMillis();// get current time in milliseconds
            String oauth_timestamp = (Long.valueOf(ts / 1000)).toString(); // then divide by 1000 to get seconds

            // the parameter string must be in alphabetical order
            // this time, I add 3 extra params to the request, "lang", "result_type" and "q".
            String parameter_string = "oauth_consumer_key=" + consumerKey + "&oauth_nonce=" + consumerSecret + "&oauth_signature_method=" + oauth_signature_method +
                    "&oauth_timestamp=" + oauth_timestamp + "&oauth_token=" +
                    TwitterAPIUtils.encode(accessToken) + "&oauth_version=1.0";
//        String twitter_endpoint = "https://api.twitter.com/1.1/statuses/user_timeline.json";
        String twitter_endpoint = urlString;
//            String twitter_endpoint = "https://stream.twitter.com/1.1/statuses/filter.json";
//            String twitter_endpoint = "https://api.twitter.com/1.1/statuses/show.json";
//        String twitter_endpoint = "https://api.twitter.com/1.1/statuses/show.json";
            String signature_base_string = get_or_post + "&"+ TwitterAPIUtils.encode(twitter_endpoint) + "&" + TwitterAPIUtils.encode(parameter_string);
            // this time the base string is signed using twitter_consumer_secret + "&" + encode(oauth_token_secret) instead of just twitter_consumer_secret + "&"
            String oauth_signature = "";
            try {
                oauth_signature = TwitterAPIUtils.computeSignature(signature_base_string, consumerSecret + "&" + TwitterAPIUtils.encode(accessTokenSecret));  // note the & at the end. Normally the user access_token would go here, but we don't know it yet for request_token
            } catch (GeneralSecurityException e) {
                e.printStackTrace();
            }
            catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            String authorizationHeader = "OAuth oauth_consumer_key=\"" + consumerKey + "\"," +
                    "oauth_nonce=\"" + consumerSecret + "\"," +
                    "oauth_signature=\"" + TwitterAPIUtils.encode(oauth_signature) + "\"," +
                    "oauth_signature_method=\"HMAC-SHA1\"," +
                    "oauth_timestamp=\"" + oauth_timestamp + "\"," +
                    "oauth_token=\"" + TwitterAPIUtils.encode(accessToken) + "\"," +
                    "oauth_version=\"1.0\"";
            return authorizationHeader;
        } else {
            return requestDTO.getTwitterConnection().getToken();
        }
    }

    public String sampleRun(TwitterRequestDTO requestDTO) throws IOException, URISyntaxException {
        isV2 = requestDTO.getTwitterConnection().getVersion().equals("2");

        ResponseDTO responseDTO = new ResponseDTO();
        String urlString = requestDTO.getTwitterConnection().getUrl().replaceAll("tweets/search/stream", "tweets/sample/stream").replaceAll(" ","");
        if (!isV2) {
            consumerKey = requestDTO.getTwitterConnection().getConsumerToken();
            consumerSecret = requestDTO.getTwitterConnection().getConsumerSecret();
            accessToken = requestDTO.getTwitterConnection().getAccessToken();
            accessTokenSecret = requestDTO.getTwitterConnection().getAccessSecret();
            urlString = requestDTO.getTwitterConnection().getUrl().replaceAll("statuses/filter.json", "statuses/sample.json").replaceAll(" ","");
            requestDTO.getTwitterConnection().setUrl(urlString);
        }
        String authorizationHeader = getAuthorizationToken(requestDTO);

        URL url = new URL(urlString);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        if (isV2) {
            connection.setRequestProperty ("Authorization", String.format("Bearer %s", authorizationHeader));
        } else {
            connection.setRequestProperty("Authorization", authorizationHeader);
        }
        responseDTO.setStatus(true);
        responseDTO.setConnection(connection);
        responseDTO.setStreamId(requestDTO.getStreamId());
        InputStream inputStream = null;
        inputStream = connection.getInputStream();
        InputStreamReader inputStreamReader = null;
        inputStreamReader = new InputStreamReader(inputStream);
        BufferedReader reader = null;
        reader = new BufferedReader(inputStreamReader);
        BufferedReader finalReader = reader;
        InputStreamReader finalInputStreamReader = inputStreamReader;
        InputStream finalInputStream = inputStream;
            try {
                String status = "";
                while (status != null) {
                    status = finalReader.readLine();
                    try {
                        System.out.println(status);
                        JSONObject tweet = new JSONObject(status);
                        if (isV2) {
                            tweet = (JSONObject) tweet.get("data");
                        } else {
                            JSONObject isDelete = null;
                            try {
                                tweet.get("delete");
                                tweet = null;
                            } catch (JSONException e) {}
                        }
                        if (tweet!=null) {
                            try {
                                System.out.println(tweet);
                                return tweet.toString().replaceAll("\"", "\\\"");
                            } catch (Exception e) {
                                System.out.println("Unable to parse status " + status);
                                e.printStackTrace();
                            }
                        }
                    } catch (Exception e){
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                System.out.println(e);
                e.printStackTrace();
            } finally {
                try {
                    connection.disconnect();
                    finalInputStreamReader.close();
                    Objects.requireNonNull(finalInputStream).close();
                    finalReader.close();
                } catch (IOException e) {
                    connection.disconnect();
                    e.printStackTrace();
                }
            }
        return "false";
    }

    private void saveTweetsToJSONFile(List<JSONObject> subQueue, String streamPath) throws IOException {
        File jsonFile = new File(streamPath + "/" + System.nanoTime() + ".json");
//        System.out.println(jsonFile.getAbsolutePath());
        if (jsonFile.createNewFile()) {
//            System.out.println("New json stream file created " + jsonFile.getName());
        }
//        org.json.simple.JSONArray tweetsList = new org.json.simple.JSONArray();
//        tweetsList.addAll(subQueue);
        //Write JSON file
//        try (FileWriter file = new FileWriter(jsonFile)) {
        try {
            FileWriter fstream = new FileWriter(jsonFile, true);
            BufferedWriter out = new BufferedWriter(fstream);
            for (JSONObject tweet: subQueue) {
                out.write(String.valueOf(tweet));
                out.newLine();
            }
            out.close();
//            file.flush();
            String[] path = jsonFile.getAbsolutePath().split("twitter_data");
            copyToS3(bucketName, "twitter_data" + path[path.length - 1], jsonFile);
            jsonFile.delete();
        } catch (IOException e) {
            e.printStackTrace();
        }
//      Dataset<Row> dataset = SparkSessionUtil.sparkSession.createDataset(queue, Encoders.STRING()).toDF();
//      dataset.createOrReplaceGlobalTempView("tweets");
    }

    public void copyToS3(String bucketName, String s3FileLocation, File sourceFileLocation) {
        AmazonS3 s3 = connectToS3();
        String path = s3FileLocation.replaceAll("\\\\", "/");
        System.out.println(path);
        System.out.println(sourceFileLocation.getAbsolutePath());
        s3.putObject(bucketName, path, sourceFileLocation);
    }

    public AmazonS3 connectToS3() {
        AWSCredentials credentials = new BasicAWSCredentials(
                accessKey,
                secretKey
        );
        AmazonS3 s3client = AmazonS3ClientBuilder
                .standard()
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .withRegion(region)
                .build();
        return s3client;
//        System.out.println(s3client);
    }

    private String createDirectory(String streamId, String directory) {
        File twitterDir = new File(tempFileLocation + "/" + directory);
        if (!twitterDir.exists()) {
            if (twitterDir.mkdir()) {
                System.out.println("Twitter Stream Directory create at location " + twitterDir.getAbsolutePath());
            }
        } else {
            System.out.println("Twitter Stream Directory already present " + twitterDir.getAbsolutePath());
        }
        long userId = JwtTokenUtil.userId;
        long clientId = JwtTokenUtil.clientId;
        File streamFolder = new File(twitterDir.getAbsolutePath() + "/" + clientId + "_" + userId + "_" + streamId);
        if (!streamFolder.exists()) {
            if (streamFolder.mkdirs()) {
                System.out.println("Directory create at location " + streamFolder.getAbsolutePath());
            }
        } else {
//            throw new Exception("Stream Id is already present");
        }
        return streamFolder.getAbsolutePath();
    }

    @Async
    public ResponseDTO runBatch(TwitterRequestDTO requestDTO) throws IOException, URISyntaxException, Exception {
        ResponseDTO responseDTO = new ResponseDTO();
        String urlString = requestDTO.getTwitterConnection().getUrl();
        String token = requestDTO.getTwitterConnection().getToken();
        String streamId = requestDTO.getStreamId();
        URIBuilder uriBuilder = new URIBuilder(urlString);
//        uriBuilder.addParameter("tweet.fields", "created_at");
        URL url = new URL(uriBuilder.toString());

        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        connection.setRequestProperty ("Authorization", String.format("Bearer %s", token));
        connection.connect();
        String streamPath = createDirectory(String.valueOf(requestDTO.getStreamId()), "twitter_batch");

        responseDTO.setStatus(true);
        responseDTO.setMessage("Tweets fetch started");
        responseDTO.setConnection(connection);
        responseDTO.setStreamId(requestDTO.getStreamId());
        responseDTO.setFileType("json");
        responseDTO.setSubmitType("s3");
        String[] path = streamPath.split("twitter_data");
        responseDTO.setFolderPath("s3a://" + bucketName + "/twitter_data" + path[path.length - 1].replaceAll("\\\\", "/"));

        Thread errThread = new Thread(() -> {
            InputStreamReader inputStreamReader = null;
            InputStream inputStream = null;
            BufferedReader reader = null;
            try {
                inputStream = (InputStream) connection.getContent();
                inputStreamReader = new InputStreamReader(inputStream);
                reader = new BufferedReader(inputStreamReader);

                String status = reader.readLine();
                System.out.println(status);
                JSONObject jsonObject = new JSONObject(status);
                JSONArray jsonArray = (JSONArray) jsonObject.get("data");
                JSONObject meta = (JSONObject) jsonObject.get("meta");
                String next_token = (String) meta.get("next_token");
                List<JSONObject> queue = new ArrayList<>();
                for(int i = 0; i < jsonArray.length(); i++) {
                    queue.add(jsonArray.getJSONObject(i));
                }
                saveTweetsToJSONFile(queue, streamPath);
                HttpURLConnection connection2 = null;
                while (next_token != null && checkConnection(streamId)) {
                    URIBuilder uriBuilder2 = new URIBuilder(urlString);
                    uriBuilder2.addParameter("next_token", next_token);
                    URL url2 = new URL(uriBuilder2.toString());

//                    connection.disconnect();
                    connection2 = (HttpURLConnection) url2.openConnection();
                    connection2.setRequestMethod("GET");
                    connection2.setRequestProperty ("Authorization", String.format("Bearer %s", token));
                    connection2.connect();
                    changeConnection(connection2, streamId);
                    inputStream = (InputStream) connection2.getContent();
                    inputStreamReader = new InputStreamReader(inputStream);
                    reader = new BufferedReader(inputStreamReader);

                    String status2 = reader.readLine();
                    JSONObject jsonObject2 = new JSONObject(status2);
                    JSONArray jsonArray2 = (JSONArray) jsonObject2.get("data");
                    JSONObject meta2 = (JSONObject) jsonObject2.get("meta");
                    next_token = (String) meta2.get("next_token");
                    List<JSONObject> queue2 = new ArrayList<>();
                    for(int i = 0; i < jsonArray.length(); i++) {
                        queue2.add(jsonArray.getJSONObject(i));
                    }
                    saveTweetsToJSONFile(queue2, streamPath);
                }
                if (connection2 != null) {
                    connection2.disconnect();
                }

            } catch (Exception e) {
                System.out.println(e);
                e.printStackTrace();
            } finally {
                try {
                    inputStreamReader.close();
                    Objects.requireNonNull(inputStream).close();
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        errThread.start();
        return responseDTO;
    }

    private boolean checkConnection(String streamId) {
        for (Iterator<Map.Entry<String, HttpURLConnection>>  entry = TwitterApiController.connectionMap.entrySet().iterator(); entry.hasNext();) {
            Map.Entry<String, HttpURLConnection> entry1 = entry.next();
            String key = entry1.getKey();
            if (key.equals(streamId)) {
                return true;
            }
        }
        return false;
    }

    private void changeConnection(HttpURLConnection connection2, String streamId) {
        for (Iterator<Map.Entry<String, HttpURLConnection>>  entry = TwitterApiController.connectionMap.entrySet().iterator(); entry.hasNext();) {
            Map.Entry<String, HttpURLConnection> entry1 = entry.next();
            String key = entry1.getKey();
            HttpURLConnection value = entry1.getValue();
            if (key.equals(streamId)) {
                value.disconnect();
                entry.remove();
            }
        }
//        for (Map.Entry<String, HttpURLConnection> entry : TwitterApiController.connectionMap.entrySet()) {
//            String key = entry.getKey();
//            HttpURLConnection value = entry.getValue();
//            if (key.equals(guid)) {
//                value.disconnect();
//                TwitterApiController.connectionMap.remove(key);
//            }
//        }
        TwitterApiController.connectionMap.put(streamId, connection2);
    }


    public void saveToDatabase() {
        Connection connection = connectToRemoteDB();
        Dataset<Row> dataset = SparkSessionUtil.sparkSession.sql("select * from global_temp.tweets");
        List<Row> statusList = dataset.collectAsList();
//        Iterator<Row> iterator = dataset.toLocalIterator();
        try {
            Statement statement = connection.createStatement();
            String values = "";
//            while(iterator.hasNext()){
//                Row row = iterator.next();
//                //do somthing
//                values += "('";
//                String value = dataset.collectAsList().get(0).toString();
//                value = row.toString().substring(15, value.length() - 1);
//                value = value.toString().replaceAll("'", "").replaceAll("\\\\\"", "\\\\\\\\\\\\\"");
//                values += value + "'),";
//            }
            for(Row status: statusList) {
                values += "('";
                String value = status.get(0).toString();
//                value = value.substring(15, value.length() - 1);
                value = value.replaceAll("'", "").replaceAll("\\\\\"", "\\\\\\\\\\\\\"");
                value = value.replaceAll("\\\\n", "");
                values += value + "'),";
            }
            values = values.substring(0, values.length() - 1);
            statement.execute("insert into twittertemp.json_data (tweetString2) values " + values);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }

    protected Connection connectToRemoteDB() {
        Connection remoteConn = null;

        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException cnf) {
            logger.info("driver could not be loaded: " + cnf);
        }

        try {
//            System.out.println("Connection JDBC URL: " + connectionInfoPOJO.getConnectionJDBCURL());
//            logger.info("Connection JDBC URL: " + connectionInfoPOJO.getConnectionJDBCURL());

            remoteConn = DriverManager.getConnection("jdbc:mysql://localhost:3306/twittertemp?serverTimezone=UTC", "root", "kaushalpc");

        } catch (SQLException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }
        return remoteConn;
    }

    /*
     * Helper method to setup rules before streaming data
     * */
    private static void setupRules(String bearerToken, TwitterRequestDTO requestDTO) throws IOException, URISyntaxException {
        List<String> existingRules = getRules(bearerToken);
        if (existingRules.size() > 0) {
            deleteRules(bearerToken, existingRules);
        }
        createRules(bearerToken, requestDTO);
    }

    /*
     * Helper method to create rules for filtering
     * */
    private static void createRules(String bearerToken, TwitterRequestDTO requestDTO) throws URISyntaxException, IOException {
        Map<String, String> rules = new HashMap<String, String>();
//        rules.put("value", "trump");
//        rules.put("tag", );
        for (Rules rules1: requestDTO.getTwitterConnection().getRules()) {
            if (rules1.getValue() != null && !rules1.getValue().isEmpty() &&
                    rules1.getTag() != null && !rules1.getTag().isEmpty()) {
                rules.put(rules1.getValue(), rules1.getTag());
            }
        }

        HttpClient httpClient = HttpClients.custom()
                .setDefaultRequestConfig(RequestConfig.custom()
                        .setCookieSpec(CookieSpecs.STANDARD).build())
                .build();

        URIBuilder uriBuilder = new URIBuilder("https://api.twitter.com/2/tweets/search/stream/rules");

        HttpPost httpPost = new HttpPost(uriBuilder.build());
        httpPost.setHeader("Authorization", String.format("Bearer %s", bearerToken));
        httpPost.setHeader("content-type", "application/json");
        StringEntity body = new StringEntity(getFormattedString("{\"add\": [%s]}", rules));
        httpPost.setEntity(body);
        HttpResponse response = httpClient.execute(httpPost);
        HttpEntity entity = response.getEntity();
        if (null != entity) {
            System.out.println(EntityUtils.toString(entity, "UTF-8"));
        }
    }

    /*
     * Helper method to get existing rules
     * */
    private static List<String> getRules(String bearerToken) throws URISyntaxException, IOException {
        List<String> rules = new ArrayList<>();
        HttpClient httpClient = HttpClients.custom()
                .setDefaultRequestConfig(RequestConfig.custom()
                        .setCookieSpec(CookieSpecs.STANDARD).build())
                .build();

        URIBuilder uriBuilder = new URIBuilder("https://api.twitter.com/2/tweets/search/stream/rules");

        HttpGet httpGet = new HttpGet(uriBuilder.build());
        httpGet.setHeader("Authorization", String.format("Bearer %s", bearerToken));
        httpGet.setHeader("content-type", "application/json");
        HttpResponse response = httpClient.execute(httpGet);
        HttpEntity entity = response.getEntity();
        if (null != entity) {
            JSONObject json = new JSONObject(EntityUtils.toString(entity, "UTF-8"));
            if (json.length() > 1) {
                JSONArray array = (JSONArray) json.get("data");
                for (int i = 0; i < array.length(); i++) {
                    JSONObject jsonObject = (JSONObject) array.get(i);
                    rules.add(jsonObject.getString("id"));
                }
            }
        }
        return rules;
    }

    /*
     * Helper method to delete rules
     * */
    private static void deleteRules(String bearerToken, List<String> existingRules) throws URISyntaxException, IOException {
        HttpClient httpClient = HttpClients.custom()
                .setDefaultRequestConfig(RequestConfig.custom()
                        .setCookieSpec(CookieSpecs.STANDARD).build())
                .build();

        URIBuilder uriBuilder = new URIBuilder("https://api.twitter.com/2/tweets/search/stream/rules");

        HttpPost httpPost = new HttpPost(uriBuilder.build());
        httpPost.setHeader("Authorization", String.format("Bearer %s", bearerToken));
        httpPost.setHeader("content-type", "application/json");
        StringEntity body = new StringEntity(getFormattedString("{ \"delete\": { \"ids\": [%s]}}", existingRules));
        httpPost.setEntity(body);
        HttpResponse response = httpClient.execute(httpPost);
        HttpEntity entity = response.getEntity();
        if (null != entity) {
            System.out.println(EntityUtils.toString(entity, "UTF-8"));
        }
    }

    private static String getFormattedString(String string, List<String> ids) {
        StringBuilder sb = new StringBuilder();
        if (ids.size() == 1) {
            return String.format(string, "\"" + ids.get(0) + "\"");
        } else {
            for (String id : ids) {
                sb.append("\"" + id + "\"" + ",");
            }
            String result = sb.toString();
            return String.format(string, result.substring(0, result.length() - 1));
        }
    }

    private static String getFormattedString(String string, Map<String, String> rules) {
        StringBuilder sb = new StringBuilder();
        if (rules.size() == 1) {
            String key = rules.keySet().iterator().next();
            return String.format(string, "{\"value\": \"" + key + "\", \"tag\": \"" + rules.get(key) + "\"}");
        } else {
            for (Map.Entry<String, String> entry : rules.entrySet()) {
                String value = entry.getKey();
                String tag = entry.getValue();
                sb.append("{\"value\": \"" + value + "\", \"tag\": \"" + tag + "\"}" + ",");
            }
            String result = sb.toString();
            return String.format(string, result.substring(0, result.length() - 1));
        }
    }

    public String getFields() {
        List<String> fields = new ArrayList<>();
        fields.add("created_at");
        fields.add("lang");
        fields.add("conversation_id");
        fields.add("author_id");
        fields.add("source");
        fields.add("reply_settings");
        fields.add("possibly_sensitive");
        fields.add("in_reply_to_user_id");
        fields.add("attachments");
        fields.add("context_annotations");
        fields.add("entities");
        fields.add("geo");
        fields.add("non_public_metrics");
        fields.add("organic_metrics");
        fields.add("promoted_metrics");
        fields.add("public_metrics");
        fields.add("referenced_tweets");
        fields.add("withheld");

        return fields.toString().replaceAll(" ", "");
    }
}
