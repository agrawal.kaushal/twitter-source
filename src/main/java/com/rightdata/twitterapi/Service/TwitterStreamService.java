package com.rightdata.twitterapi.Service;

import com.rightdata.twitterapi.Model.TwitterRequestDTO;
import com.rightdata.twitterapi.Utils.SparkSessionUtil;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.spark.SparkConf;
import org.apache.spark.SparkContext;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.VoidFunction;
import org.apache.spark.sql.*;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructType;
import org.apache.spark.streaming.Duration;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaReceiverInputDStream;
import org.apache.spark.streaming.twitter.TwitterUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;
import twitter4j.*;
import twitter4j.conf.ConfigurationBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import java.util.*;
import java.util.concurrent.Future;
import java.util.function.Function;

import org.apache.spark.streaming.api.java.JavaStreamingContext;

@Service
public class TwitterStreamService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

//    @Value("${oauth.consumerKey}")
//    private String consumerKey;
//    @Value("${oauth.consumerSecret}")
//    private String consumerSecret;
//    @Value("${oauth.accessToken")
//    private String accessToken;
//    @Value("${oauth.accessTokenSecret}")
//    private String accessTokenSecret;

    private SparkSession sparkSession;

    public TwitterStream startStreamingTweets(TwitterRequestDTO requestDTO) throws IOException, URISyntaxException {
//        Producer producer = getProducer();
        ConfigurationBuilder cb = getConfiguration();
//        sparkStreamTwitter();
//        connectStream();

         return streamFeed(cb, requestDTO);
//        List<String> tweet = twitter.getHomeTimeline().stream()
//                .map(item -> item.getText())
//                .collect(Collectors.toList());
//        System.out.println(tweet);
//        TwitterClient twitterClient = new TwitterClient(TwitterCredentials.builder()
//                .accessToken(oauth.consumerKey)
//                .accessTokenSecret("<secret_token>")
//                .apiKey("<api_key>")
//                .apiSecretKey("<secret_key>")
//                .build());
//        Authentication auth = new OAuth2

    }

    private void sparkStreamTwitter() {

        System.setProperty("twitter4j.oauth.consumerKey", "t35z6Nl82swmsPoEw1mWxiUSb");
        System.setProperty("twitter4j.oauth.consumerSecret", "53Qs77bvYdyRb9xUz67eW2ePmAd0z7muMNgFUQbuD8tRyece5L");
        System.setProperty("twitter4j.oauth.accessToken", "142020510-jNJqB3Ucdn57C2dD7EyDMxkNcOy8s8cmdRy6os9b");
        System.setProperty("twitter4j.oauth.accessTokenSecret", "zZIwVHGVHBvnrry2ttu2MvULJY86xZswsJlItPbTxvZQD");

//        SparkConf sparkConf = SparkSessionUtil.sparkConf;
////        JavaSparkContext jsc = new JavaSparkContext(sparkConf);
////        JavaStreamingContext ssc = new JavaStreamingContext(sparkConf, Durations.seconds(40));
//        JavaSparkContext ctx = JavaSparkContext.fromSparkContext(SparkContext.getOrCreate());
//        JavaStreamingContext jsc = new JavaStreamingContext(ctx, new Duration(10000));
////        JavaStreamingContext ssc = new JavaStreamingContext(jsc, Durations.seconds(10));

//        SparkConf conf = new SparkConf().setMaster("local[*]").setAppName("sparkStreamIng");
        SparkConf conf = SparkSessionUtil.sparkConf;
        JavaStreamingContext jsc = new JavaStreamingContext(conf, Durations.seconds(1));


        JavaReceiverInputDStream<Status> stream = TwitterUtils.createStream(jsc);

        JavaDStream<String> english = stream.filter((f)->f.getLang().equals("en"))
                .map((status)-> status.getText().replaceAll("[^\\x00-\\x7F]", "").replace("\n", " "));
        english.print();

        english.foreachRDD(new VoidFunction<JavaRDD<String>>() {
            @Override
            public void call(JavaRDD<String> stringJavaRDD) throws Exception {
                Dataset<Row> rowDataset = SparkSessionUtil.sparkSession.read().json(stringJavaRDD);
                rowDataset.createOrReplaceTempView("temp1");

            }
//                JavaRDD<Row> rowRDD = (JavaRDD<Row>) stringJavaRDD.map(new Function<String, Row>() {
//                    @Override
//                    public Row apply(String s) {
//                        return null;
//                    }
//
//                    @Override
//                    public <V> Function<V, Row> compose(Function<? super V, ? extends String> before) {
//                        return null;
//                    }
//
//                    @Override
//                    public <V> Function<String, V> andThen(Function<? super Row, ? extends V> after) {
//                        return null;
//                    }
//        });
//
//                    // Get the singleton instance of SQLContext
//                    SQLContext sqlContext = SQLContext.getOrCreate(rdd.context());
//
//                    // Convert RDD[String] to RDD[case class] to DataFrame
//                    JavaRDD<JavaRow> rowRDD = rdd.map(word -> {
//                        JavaRow record = new JavaRow();
//                        record.setWord(word);
//                        return record;
//                    });
//                    DataFrame wordsDataFrame = sqlContext.createDataFrame(rowRDD, JavaRow.class);
//
//                    // Register as table
//                    wordsDataFrame.registerTempTable("words");
//
//                    // Do word count on table using SQL and print it
//                    DataFrame wordCountsDataFrame =
//                            sqlContext.sql("select word, count(*) as total from words group by word");
//
//                    wordCountsDataFrame.show();
//                    return null;
                });


//        english.foreachRDD(new VoidFunction<JavaRDD<String>>() {
//             @Override
//             public void call(JavaRDD<String> rdd) {
//                 JavaRDD<Row> rowRDD = rdd.map(new Function<String, Row>() {
//                     @Override
//                     public Row apply(String msg) {
//                         Row row = RowFactory.create(msg);
//                         return row;
//                     }
//
//                     @Override
//                     public <V> Function<V, Row> compose(Function<? super V, ? extends String> before) {
//                         return null;
//                     }
//
//                     @Override
//                     public <V> Function<String, V> andThen(Function<? super Row, ? extends V> after) {
//                         return null;
//                     }
//
////                     @Override
////                     public Row call(String msg) {
////                         Row row = RowFactory.create(msg);
////                         return row;
////                     }
//                 });
//                 //Create Schema
//                 StructType schema = DataTypes.createStructType(new StructField[]{DataTypes.createStructField("Message", DataTypes.StringType, true)});
//                 //Get Spark 2.0 session
//                 SparkSession spark = JavaSparkSessionSingleton.getInstance(rdd.context().getConf());
//                 Dataset<Row> msgDataFrame = spark.createDataFrame(rowRDD, schema);
//                 msgDataFrame.show();
//             }
//         });

        jsc.start();
//        try {
//            jsc.awaitTermination();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }

    }

    @Async
    public TwitterStream streamFeed(ConfigurationBuilder cb, TwitterRequestDTO requestDTO) {
        List<String> queue = new ArrayList<>();
        StatusListener listener = new StatusListener() {
            int i = 0;
            long time = new Date().getTime()/ 1000;
            @Override
            public void onStatus(Status status) {
                if (Thread.currentThread().isInterrupted()) {
                    System.out.println("Cancelled");
//                    inProgress.set(false);
//                    return new AsyncResult<String>("Cancelled Task");
                }
                System.out.println("@" + status);
                queue.add(new JSONObject(status).toString());
                long currentTime = new Date().getTime()/ 1000;
                long count = requestDTO.getTwitterConnection().getMaxBatchSize() != 0 ? requestDTO.getTwitterConnection().getMaxBatchSize() : 100;
                i = i + 1;
                if (false) {
                    if (i % count == 0) {
                        List<String> subQueue = queue.subList(0, Math.toIntExact(count));
                        System.out.println("inside count interval" + i);
                        Dataset<Row> dataset = SparkSessionUtil.sparkSession.createDataset(queue, Encoders.STRING()).toDF();
                        dataset.createOrReplaceGlobalTempView("tweets");
                        subQueue.clear();
                        saveToDatabase();
//                    saveToSpark(queue);
                    }
                } else if (currentTime - time > requestDTO.getTwitterConnection().getTimeInterval() || i % 100 == 0) {
                    currentTime = new Date().getTime()/ 1000;
                    time = new Date().getTime()/ 1000;
                    System.out.println("inside time interval" + i);
                    int length = queue.size();
                    List<String> subQueue = queue.subList(0, length);
                    Dataset<Row> dataset = SparkSessionUtil.sparkSession.createDataset(queue, Encoders.STRING()).toDF();
                    dataset.createOrReplaceGlobalTempView("tweets");
                    subQueue.clear();
                    saveToDatabase();
                }
            }

            @Override
            public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {
                System.out.println("Got a status deletion notice id:" + statusDeletionNotice.getStatusId());
            }

            @Override
            public void onTrackLimitationNotice(int numberOfLimitedStatuses) {
                System.out.println("Got track limitation notice:" + numberOfLimitedStatuses);
            }

            @Override
            public void onScrubGeo(long userId, long upToStatusId) {
                System.out.println("Got scrub_geo event userId:" + userId + " upToStatusId:" + upToStatusId);
            }

            @Override
            public void onStallWarning(StallWarning warning) {
                System.out.println("Got stall warning:" + warning);
            }

            @Override
            public void onException(Exception ex) {
                ex.printStackTrace();
            }
        };

        TwitterStream twitterStream = new TwitterStreamFactory(cb.build()).getInstance();
        twitterStream.addListener(listener);

//        try {
//            SparkSessionUtil.startSession(String.valueOf(twitterStream.getId()));
//            this.sparkSession = SparkSessionUtil.sparkSession;
//            sparkSession.sql("create table table1 (c1 string) using delta");
//        } catch (TwitterException e) {
//            e.printStackTrace();
//        }

        FilterQuery filterQuery = new FilterQuery();
//        filterQuery.track("biden");
//        if (requestDTO.isBatchMode() && requestDTO.getBatchSize() != 0) {
//            filterQuery.count((int) requestDTO.getBatchSize());
//        }
        if (requestDTO.getTwitterConnection().getFollow() != null && requestDTO.getTwitterConnection().getFollow().length > 0) {
            filterQuery.follow(requestDTO.getTwitterConnection().getFollow());
        }
        if (requestDTO.getTwitterConnection().getTrack() != null && requestDTO.getTwitterConnection().getTrack().length > 0) {
            filterQuery.track(requestDTO.getTwitterConnection().getTrack());
        }
        if (requestDTO.getTwitterConnection().getLocations() != null && requestDTO.getTwitterConnection().getLocations().length > 0) {
            filterQuery.locations(requestDTO.getTwitterConnection().getLocations());
        }
        if (requestDTO.getTwitterConnection().getLanguage() != null && requestDTO.getTwitterConnection().getLanguage().length > 0) {
            filterQuery.language(requestDTO.getTwitterConnection().getLanguage());
        }
        if (requestDTO.getTwitterConnection().getFilterLevel() != null && requestDTO.getTwitterConnection().getFilterLevel().length() > 0) {
            filterQuery.filterLevel(requestDTO.getTwitterConnection().getFilterLevel());
        }

        twitterStream.filter(filterQuery);

        return twitterStream;
//        return new AsyncResult<String>("tasksList");
    }

    private void saveToSpark(List<Status> queue) {
        List<Status> subQueue = queue.subList(0, 10);
        String values = " ";

        for(Status status: subQueue) {
            values += "('";
            String value = new JSONObject(status).toString().replaceAll("'", "").replaceAll("\\\\\"", "\\\\\\\\\\\\\"");
            values += value + "'),";
        }
        values = values.substring(0, values.length() - 1);
        String sql = "insert into table1 values " + values;
        Dataset<Row> result = sparkSession.sql(sql);
        subQueue.clear();
    }

    public ConfigurationBuilder getConfiguration() {
        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setDebugEnabled(true)
                .setOAuthConsumerKey("BBinVNeMrEYPl6TsDtu9KhLMq")
                .setOAuthConsumerSecret("vyinZ1v5BjrkJTn3904iKShjbZyMbRJhvBsGFTDMFxIH3Aewui")
                .setOAuthAccessToken("1221765625-EoOWtoKuu3N9tD1CKrAGvRxWg5WFSdNKYFWfLsM")
                .setOAuthAccessTokenSecret("RN4YZI1BTsJBp97WRjIOtCA6VEeKW1iT1HrUVnJuewLVh")
                .setTweetModeExtended(true);
        return cb;
//        TwitterFactory tf = new TwitterFactory(cb.build());
//        return tf.getInstance();
    }

    public void saveToDatabase() {
        Connection connection = connectToRemoteDB();
        Dataset<Row> dataset = SparkSessionUtil.sparkSession.sql("select * from global_temp.tweets");
        List<Row> statusList = dataset.collectAsList();
//        Iterator<Row> iterator = dataset.toLocalIterator();
        try {
            Statement statement = connection.createStatement();
            String values = "";
//            while(iterator.hasNext()){
//                Row row = iterator.next();
//                //do somthing
//                values += "('";
//                String value = dataset.collectAsList().get(0).toString();
//                value = row.toString().substring(15, value.length() - 1);
//                value = value.toString().replaceAll("'", "").replaceAll("\\\\\"", "\\\\\\\\\\\\\"");
//                values += value + "'),";
//            }
            for(Row status: statusList) {
                values += "('";
                String value = status.get(0).toString();
//                value = value.substring(15, value.length() - 1);
                value = value.replaceAll("'", "").replaceAll("\\\\\"", "\\\\\\\\\\\\\"");
                value = value.replaceAll("\\\\n", "");
                values += value + "'),";
            }
            values = values.substring(0, values.length() - 1);
            statement.execute("insert into twittertemp.json_data (tweetString2) values " + values);
        } catch (SQLException throwables) {
             throwables.printStackTrace();
        }

    }

    public Producer getProducer() {
        Properties props = new Properties();
        props.put("metadata.broker.list", "");
        props.put("serializer.class", "kafka.serializer.StringEncoder");

//        ProducerConfig producerConfig = new ProducerConfig(props);

//        Producer<String, String> producer = new KafkaProperties.Producer<String, String>(producerConfig);

        KafkaProducer<String, String> producer = new KafkaProducer<String, String>(props);

        return producer;
    }


    // To set your enviornment variables in your terminal run the following line:
    // export 'BEARER_TOKEN'='<your_bearer_token>'

//        public void stream() throws IOException, URISyntaxException {
//            String bearerToken = System.getenv("BEARER_TOKEN");
//            if (null != bearerToken) {
//                connectStream(bearerToken);
//            } else {
//                System.out.println("There was a problem getting you bearer token. Please make sure you set the BEARER_TOKEN environment variable");
//            }
//
//        }

    /*
     * This method calls the sample stream endpoint and streams Tweets from it
     * */
    private void connectStream() throws IOException, URISyntaxException {


        URL url = new URL ("https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=twitterdev&count=10");
        String encoding = Base64.getEncoder().encodeToString(("7aU76KfudX4uqETptHsks1eFN:ePsbiFhy6N0ObPHfcNW5Up8sm8KQ0SRumrKx3rFIUiD0bGXkJ8").getBytes("UTF-8"));

        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        connection.setDoOutput(true);
        connection.setRequestProperty("Authentication", "Basic " + encoding);
//            connection.setRequestProperty("Authorization", "Bearer " + "AAAAAAAAAAAAAAAAAAAAAAuvMwEAAAAAONk7l4%2Bt%2B6Bl6mqn36N3sBallzU%3DI4TgbPo0gZ2mZJgx5fOluei1XhiV1Ibsw8AsXatw7eC5ihDOlS");
        InputStream content = (InputStream)connection.getContent();
        BufferedReader in   =
                new BufferedReader (new InputStreamReader (content));
        String line;
        while ((line = in.readLine()) != null) {
            System.out.println(line);
        }



//            HttpClient httpclient = new java.net.http.HttpClient();
//            String encoding = BASE64Encoder.encode(user + ":" + pwd);
//            HttpGet httpGet = new HttpGet("http://host:post/test/login");
//            httpGet.setHeader(HttpHeaders.AUTHORIZATION, "Basic " + encoding);
//
//            System.out.println("executing request " + httpGet.getRequestLine());
//            HttpResponse response = httpClient.execute(httpPost);
//            HttpEntity entity = response.getEntity();


//            CloseableHttpClient httpClient = HttpClients.custom()
//                    .setDefaultRequestConfig(RequestConfig.custom()
//                            .setCookieSpec(CookieSpecs.STANDARD).build())
//                    .build();
//
//            URIBuilder uriBuilder = new URIBuilder("https://api.twitter.com/2/tweets/sample/stream");
//
//            HttpGet httpGet = new HttpGet(uriBuilder.build());
//            httpGet.setHeader("Authorization", String.format("Bearer %s", "AAAAAAAAAAAAAAAAAAAAAAuvMwEAAAAAHy5ijikI5lsnPMs7L4HA%2Fx4o%2FOw%3D7pNtbvGsYdCXtpddBHpxY6OkzJEsMazwWnZvdTLW4hAsYJYDgU"));
//
//            CloseableHttpResponse response = httpClient.execute(httpGet);
//            HttpEntity entity = response.getEntity();
//            if (null != entity) {
//                BufferedReader reader = new BufferedReader(new InputStreamReader((entity.getContent())));
//                String line = reader.readLine();
//                while (line != null) {
//                    System.out.println(line);
//                    line = reader.readLine();
//                }
//            }

    }

    protected Connection connectToRemoteDB() {
        Connection remoteConn = null;

        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException cnf) {
            logger.info("driver could not be loaded: " + cnf);
        }

        try {
//            System.out.println("Connection JDBC URL: " + connectionInfoPOJO.getConnectionJDBCURL());
//            logger.info("Connection JDBC URL: " + connectionInfoPOJO.getConnectionJDBCURL());

            remoteConn = DriverManager.getConnection("jdbc:mysql://localhost:3306/twittertemp?serverTimezone=UTC", "root", "kaushalpc");

        } catch (SQLException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }
        return remoteConn;
    }

}

