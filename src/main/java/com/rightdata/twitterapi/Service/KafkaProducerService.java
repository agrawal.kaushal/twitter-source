package com.rightdata.twitterapi.Service;

import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.security.Timestamp;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.Date;
import java.util.Properties;

@Service
public final class KafkaProducerService {
    private static final Logger logger = LoggerFactory.getLogger(KafkaProducerService.class);
    private static Producer<String, String> producer;
//    private final KafkaTemplate<String, String> kafkaTemplate;
//    private final String TOPIC = "kafkaTopic";

//    public KafkaProducerService(KafkaTemplate<String, String> kafkaTemplate) {
//        this.kafkaTemplate = kafkaTemplate;
//    }
//    public void sendMessage(String message) {
//        logger.info(String.format("$$$$ => Producing message: %s", message));
//
//        ListenableFuture<SendResult<String, String>> future = this.kafkaTemplate.send(TOPIC, message);
//        future.addCallback(new ListenableFutureCallback<>() {
//            @Override
//            public void onSuccess(Object o) {
//
//            }
//
//            @Override
//            public void onFailure(Throwable ex) {
//                logger.info("Unable to send message=[ {} ] due to : {}", message, ex.getMessage());
//            }
//        });
//    }


    public static void kafkaProducer() {


        //Assign topicName to string variable
        String topicName = "streamID3";
//        Thread.currentThread().setContextClassLoader(null);

        // create instance for properties to access producer configs
        Properties props = new Properties();

        //Assign localhost id
        props.put("bootstrap.servers", "http://54.193.118.92:9092");
//        props.put("group.id", "Dextrus");
//        props.put("producer.type","async");

        //Set acknowledgements for producer requests.
//        props.put("acks", "1");

        //If the request fails, the producer can automatically retry,
//        props.put("retries", 0);

        //Specify buffer size in config
//        props.put("batch.size", 16384);
//        props.put("batch.size", 16);

        //Reduce the no of requests less than 0
//        props.put("linger.ms", 1);

        //The buffer.memory controls the total amount of memory available to the producer for buffering.
//        props.put("buffer.memory", 33554432);

        props.put("serializer.class",
                "org.apache.kafka.common.serialization.StringSerializer");
        props.put("key.serializer",
                "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer",
                "org.apache.kafka.common.serialization.StringSerializer");

        try {


            producer = new KafkaProducer<String, String>(props);
//        producer.send(new ProducerRecord<String, String>(topicName, "key","value"));
//            int i = 0;
////        while (true) {
//            i++;
//            System.out.println("start");
//            producer.send(new ProducerRecord<String, String>(topicName, "6"));
////            RecordMetadata pro = producer.send(new ProducerRecord<String, String>(topicName, "key" + i, "value")).get();
//            System.out.println(i);
//            producer.flush();
//            System.out.println(i);
////        }
//            producer.close();
            System.out.println("Kafka Producer Started");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void sendMessage(String topicName, String stepId, String streamId, String count) throws Exception {
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");
        String formattedDate = sdf.format(date);
        String message = "streamId:::"+streamId+"|targetId:::"+stepId+"|sourceCount:::"+count+"|Metrics Timestamp:::"+formattedDate+"";
        producer.send(new ProducerRecord<String, String>(topicName, message));
        System.out.println("Message sent successfully");
    }

    public static void kafkaConsumer() throws Exception {
        //Kafka consumer configuration settings
        String topicName = "streamID";
        Properties props = new Properties();

        props.put("bootstrap.servers", "18.216.214.109:9092");
        props.put("group.id", "Dextrus");
        props.put("enable.auto.commit", "true");
        props.put("auto.commit.interval.ms", "1000");
        props.put("session.timeout.ms", "30000");
        props.put("key.deserializer",
                "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("value.deserializer",
                "org.apache.kafka.common.serialization.StringDeserializer");
        KafkaConsumer<String, String> consumer = new KafkaConsumer
                <String, String>(props);

        //Kafka Consumer subscribes list of topics here.
        consumer.subscribe(Arrays.asList(topicName));

        //print the topic name
        System.out.println("Subscribed to topic " + topicName);
        int i = 0;

        while (true) {
            i++;
//            producer.send(new ProducerRecord<String, String>(topicName, "key" + i, "value"));
//            producer.flush();
            System.out.println(i);
//            ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(1000));
//            System.out.println(records.count());
//            for (ConsumerRecord<String, String> record : records)

                // print the offset,key and value for the consumer records.
//                System.out.printf("offset = %d, key = %s, value = %s\n",
//                        record.offset(), record.key(), record.value());
        }
    }

}
