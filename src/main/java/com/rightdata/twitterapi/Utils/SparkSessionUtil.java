package com.rightdata.twitterapi.Utils;

import org.apache.commons.io.FileUtils;
import org.apache.spark.SparkConf;
import org.apache.spark.SparkContext;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.streaming.Durations;
//import org.apache.tomcat.util.http.fileupload.FileUtils;

import java.io.File;
import java.io.IOException;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.streaming.api.java.JavaStreamingContext;

public class SparkSessionUtil {

    public static SparkSession sparkSession;
    public static String warehouseLocation = "spark-warehouse2";
    //    public static String warehouseLocation = "s3a://dextrus.dec.eks/delta";
    public static SparkContext sparkContext;
    public static SparkConf sparkConf;
    public static JavaStreamingContext jsc;

    public static void startSession(String location) {
        File file = new File(warehouseLocation + "/location");
        warehouseLocation = file.getAbsolutePath();
        try {
            FileUtils.deleteDirectory(file);
        } catch (IOException e) {
            System.out.println("Cant find spark warehouse");
        }
//        setSparkConf();
//        SparkSession session =  SparkSession.builder().sparkContext(sparkContext).enableHiveSupport().getOrCreate().newSession();
//        System.out.println(session.conf().get("spark.sql.catalogImplementation"));
//        sparkSession = session;
//        System.out.println(warehouseLocation);

//        setSparkConfig();
//        sparkSession =  SparkSession.builder().appName("sparkSession").master("local[2]").getOrCreate();
        sparkSession = SparkSession.builder().appName("Reading and writing to delta")
                .master("local[*]")
                .config("spark.sql.warehouse.dir", warehouseLocation)
                .config("spark.sql.extensions", "io.delta.sql.DeltaSparkSessionExtension")
                .config("spark.sql.catalog.spark_catalog", "org.apache.spark.sql.delta.catalog.DeltaCatalog")
                .getOrCreate().newSession();

//        sparkSession = SparkSession.builder().appName("Reading and writing to delta")
//                .master("k8s://http://localhost:8001")
//                .config("spark.kubernetes.container.image", "290384616740.dkr.ecr.us-east-1.amazonaws.com/rightdata/spark:v3.0.1-hive-s3")
//                .config("spark.sql.warehouse.dir", "s3a://dextrus.dec.eks/user/hive/warehouse")
//                .config("spark.hadoop.fs.s3a.access.key","AKIAUHHCFTUSLFIJTGA7")
//                .config("spark.hadoop.fs.s3a.secret.key", "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")
//                .config("spark.sql.extensions", "io.delta.sql.DeltaSparkSessionExtension")
//                .config("spark.sql.catalog.spark_catalog", "org.apache.spark.sql.delta.catalog.DeltaCatalog")
//                .getOrCreate().newSession();
    }

//    public static void setSparkConfig() {
//        sparkConf = new SparkConf().setAppName("TwitterPopularTags").setMaster("local[1]");
//        jsc = new JavaStreamingContext(sparkConf, Durations.seconds(1));
////        JavaSparkContext ctx = new JavaSparkContext(sparkConf);
////        sparkContext = ctx.sc();
//    }

    private static void setSparkConf(){
        System.out.println("in config");
        /*System.setProperty("java.security.krb5.conf", env.getProperty("security.krb5_conf"));
        Configuration conf = new Configuration();
        conf.set("hadoop.security.authentication", env.getProperty("hadoop.security_authentication"));
        conf.set("hadoop.security.authorization", env.getProperty("hadoop.security_authorization"));
        conf.set("fs.defaultFS", env.getProperty("hadoop.fs_defaultFS"));
        conf.set("hadoop.rpc.protection", env.getProperty("hadoop.rpc_protection"));
        conf.set("dfs.namenode.kerberos.principal",env.getProperty("hadoop.dfs_namenode_kerberos_principal"));
        conf.set("spark.yarn.principal", env.getProperty("spark.yarn_principal"));
        UserGroupInformation.setConfiguration(conf);
        try {
            UserGroupInformation.loginUserFromKeytab(env.getProperty("spark.yarn_principal"),
                    env.getProperty("spark.keytab_path"));
        } catch (IOException e) {
            e.printStackTrace();
        }*/
        SparkConf sparkConf = new SparkConf().setAppName("Dextrusdbexplorer")
                .setMaster("k8s://https://2E8447734AB685363367D73CE9F18409.gr7.eu-central-1.eks.amazonaws.com")
                .set("spark.kubernetes.container.image", "290384616740.dkr.ecr.us-east-1.amazonaws.com/rightdata/spark:v3.0.1-hive-s3")
                //.set("spark.driver.extraClassPath", env.getProperty("spark.driver_extraClassPath"))
                //.set("spark.kubernetes.kerberos.enabled", env.getProperty("spark.kubernetes_kerberos_enabled"))
                .set("spark.kubernetes.executor.deleteOnTermination", "false")
                .set("spark.driver.host", "10.0.0.94")
                .set("spark.driver.port", "44103")
                .set("spark.blockManager.port","61023")
                .set("spark.port.maxRetries","20")
                //.set("spark.kerberos.access.hadoopFileSystems", env.getProperty("spark.kerberos_access_hadoopFileSystems"))
                //.set("spark.kerberos.keytab", env.getProperty("spark.kerberos_keytab"))
                //.set("spark.kerberos.principal",env.getProperty("spark.kerberos_principal"))
                //.set("spark.kubernetes.kerberos.krb5.path",env.getProperty("spark.kubernetes_kerberos_krb5_path"))
                //.set("spark.driver.memory", env.getProperty("spark.driver_memory"))
                //.set("spark.executor.memory", env.getProperty("spark.executor_memory"))
                //.set("spark.driver.memoryOverhead",env.getProperty("spark.driver_memoryOverhead"))
                //.set("spark.executor.memoryOverhead",env.getProperty("spark.executor_memoryOverhead"))
                //.set("spark.dynamicAllocation.enabled", env.getProperty("spark.dynamicAllocation_enabled"))
                //.set("spark.dynamicAllocation.shuffleTracking.enabled", env.getProperty("spark.dynamicAllocation_shuffleTracking_enabled"))
                //.set("spark.dynamicAllocation.executorIdleTimeout", env.getProperty("spark.dynamicAllocation_executorIdleTimeout"))
                //.set("spark.dynamicAllocation.schedulerBacklogTimeout", env.getProperty("spark.dynamicAllocation_schedulerBacklogTimeout"))
                //.set("spark.executor.cores",env.getProperty("spark.executor_cores"))
                //.set("spark.dynamicAllocation.minExecutors",env.getProperty("spark.dynamicAllocation_minExecutors"))
                //.set("spark.dynamicAllocation.maxExecutors",env.getProperty("spark.dynamicAllocation_maxExecutors"))
                .set("spark.sql.warehouse.dir","s3a://dextrus.dec.eks/delta")
                //.set("spark.hadoop.fs.s3a.impl", "org.apache.hadoop.fs.s3.S3FileSystem")
                .set("spark.hadoop.fs.s3a.access.key","AKIAUHHCFTUSLFIJTGA7")
                .set("spark.hadoop.fs.s3a.secret.key", "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")
                .set("hive.metastore.warehouse.dir", "s3a://dextrus.dec.eks/hive")
                .set("spark.hadoop.fs.s3a.endpoint", "s3.eu-central-1.amazonaws.com")
//                .set("spark.jars.packages", "io.delta:delta-core_2.12:0.7.0")
                .set("spark.sql.extensions", "io.delta.sql.DeltaSparkSessionExtension")
                .set("spark.sql.catalog.spark_catalog", "org.apache.spark.sql.delta.catalog.DeltaCatalog")
                .set("spark.jars", "s3a://dextrus.dec.eks/delta-core_2.12-0.7.0.jar");
        //.set("spark.jars", "s3a://dextrus.dec.eks/snowflake-jdbc-3.12.16.jar,s3a://dextrus.dec.eks/spark-snowflake_2.12-2.8.3-spark_3.0.jar,s3a://dextrus.dec.eks/spark-redshift_2.11-3.0.0-preview1.jar,s3a://dextrus.dec.eks/mysql-connector-java-8.0.20.jar");
        JavaSparkContext ctx = new JavaSparkContext(sparkConf);
        sparkContext = ctx.sc();
        //System.out.println(""+  sparkContext.getConf().get("spark.sql.catalogImplementation"));

    }

    public static void closeSession() {
        sparkSession.close();
    }
}
