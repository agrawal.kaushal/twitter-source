import org.destrux.streaming.StructuredStreamingTransform;

import java.util.HashMap;
import java.util.Map;

public class Src_KF_to_JDBC_SNF_Tgt {
    public static void main(String[] args) {
        // spark conf
        Map<String, String> sparkuserConf = new HashMap<>();
        sparkuserConf.put("spark.app.name", "Kafka_to_Snowflake_wrapper_test");
        sparkuserConf.put("spark.cores.max", "2");
        sparkuserConf.put("spark.driver.memory", "1g");
        sparkuserConf.put("spark.executor.memory", "2g");
        sparkuserConf.put("spark.master", "local[2]");
        sparkuserConf.put("spark.sql.shuffle.partitions", "1");
        sparkuserConf.put("spark.jars", "/home/ec2-user/test_streaming/sparkstreaming-driver-destrux-1.0.jar");
        sparkuserConf.put("spark.dynamicAllocation.enabled","true");
        sparkuserConf.put("spark.shuffle.service.enabled","true");
        sparkuserConf.put("spark.driver.extraJavaOptions","-Dlog4jspark.root.logger=INFO,console");

        System.out.println("//======hadoop conf=======");
        // hadoop conf
        Map<String, String> hadoopUserConf = new HashMap<>();

        System.out.println("//=======get sparktransform instance will be intialized only once even on multiple calls=======");

        StructuredStreamingTransform sparkTransform = StructuredStreamingTransform.getInstance(sparkuserConf, hadoopUserConf);

        System.out.println("\n******* Spark Test connection is completed ******\n ");
        String[] SRC_kafka_source = new String[]{
                "--tempTablename","temp","--input_topics","2659062165720963a461034c0353497e__world_city_","--bootstrap.servers","3.139.95.147:9092","","--startingOffsets","latest","--kafkaGroupID","Dextrus","--failOnDataLoss","FALSE","--avroSchemaFromFileFlag","false","--schemaRegistryURL","http://3.139.95.147:8081"

        };
        sparkTransform.loadStreamSource(SRC_kafka_source, "KAFKA", "AVRO");

        String[] transformations = new String[]{
                "--sql","SELECT ID,lower(Name) as Name,lower(CountryCode) as CountryCode,District, Population,before,op FROM global_temp.temp","--tempTablename","temp1"
        };
        sparkTransform.transformation(transformations);

        String[] tgt_params = new String[]{
                "--final_sql","SELECT ID,Name,CountryCode,District,Population,before,op FROM global_temp.temp1","--jdbcSinkType","Snowflake","--DBJdbc_url","jdbc:snowflake://bja71088.us-east-1.snowflakecomputing.com","--DB_name","DEMO_DB","--DB_username","dextrus","--DB_password","Dextrus!1","--DB_tablename","DEMO_DB.PUBLIC.CITY_TAB","--checkpointLocation","/tmp/cp/FileTest40","--outputMode","append","--delimiter","--trigger","5 seconds","--pk_column_name","ID","--columns_name_list","ID,Name,CountryCode,District,Population","--query_name","stream_oracle_demo"
        };
        sparkTransform.targetLoad("JDBC", tgt_params);
        System.out.println("\n******* Spark Streaming Started ******\n "+ sparkTransform.getAppURL());
        sparkTransform.termination();
        System.out.println("\n******* Spark Streaming Stooped******\n ");
        Runtime r = Runtime.getRuntime();
        r.addShutdownHook(new Thread(() -> System.out.println("shut down hook task completed..")));
    }
}
