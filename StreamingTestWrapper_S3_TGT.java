import org.destrux.streaming.StructuredStreamingTransform;

import java.util.HashMap;
import java.util.Map;

public class StreamingTestWrapper_S3_TGT {
    public static void main(String[] args) {
        // spark conf
        Map<String, String> sparkuserConf = new HashMap<>();
        sparkuserConf.put("spark.app.name", "Kafka_to_Snowflake_wrapper_test");
        sparkuserConf.put("spark.cores.max", "2");
        sparkuserConf.put("spark.driver.memory", "1g");
        sparkuserConf.put("spark.executor.memory", "2g");
        sparkuserConf.put("spark.master", "local[2]");
        sparkuserConf.put("spark.sql.shuffle.partitions", "1");
        sparkuserConf.put("spark.jars", "/home/ec2-user/test_streaming/sparkstreaming-driver-destrux-1.0.jar");
        sparkuserConf.put("spark.dynamicAllocation.enabled","true");
        sparkuserConf.put("spark.shuffle.service.enabled","true");

        System.out.println("//======hadoop conf=======");
        // hadoop conf
        Map<String, String> hadoopUserConf = new HashMap<>();

        System.out.println("//=======get sparktransform instance will be intialized only once even on multiple calls=======");

        StructuredStreamingTransform sparkTransform = StructuredStreamingTransform.getInstance(sparkuserConf, hadoopUserConf);

        System.out.println("\n******* Spark Test connection is completed ******\n ");
        String[] SRC_kafka_source = new String[]{
                "--tempTablename", "temp",
                "--input_topics", "b7e662e90b27b343415f4622a9add9b5__world_city_",
                "--bootstrap.servers", "3.139.95.147:9092",
                "--startingOffsets", "earliest",
                "--kafkaGroupID", "Dextrus",
                "--failOnDataLoss", "FALSE",
                "--avroSchemaFromFileFlag", "false",
                "--schemaRegistryURL", "http://3.139.95.147:8081",

                //"--avroSchemFilePath","person.avsc"

        };
        sparkTransform.loadStreamSource(SRC_kafka_source, "KAFKA", "AVRO");


        String[] transformations_file = new String[]{
                "--sql", "SELECT ID,lower(Name) as Name,lower(CountryCode) as CountryCode,District, Population, before,op FROM global_temp.temp",
                "--tempTablename", "temp1"
        };
        sparkTransform.transformation(transformations_file);
        String[] TGT_File = new String[]{

                "--final_sql", "SELECT * FROM global_temp.temp1",
                "--file_format","json",
                "--checkpointLocation", "s3://snf-stream-files/cp2/",
                "--accessKey","AKIAUHHCFTUSBAOC3L77",
                "--secretKey","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",
                "--trigger", "5 seconds",
                "--File_output_path", "s3://snf-stream-files/output2/",
                "--delimiter", "|",
                "--escape", "\\",
                "--quoteChar", "\"",
                "--outputMode","append",
                "--query_name","File_target",
                "--metric_bootstapservers","3.139.95.147:9092",
                "--metric_topic","metricsTopic",
                "--streamId","101",
                "--targetId","105"
        };
        sparkTransform.targetLoad("FILE", TGT_File);
        System.out.println("\n******* Spark Streaming Started ******\n " + sparkTransform.getAppURL());
        sparkTransform.termination();
        System.out.println("\n******* Spark Streaming Stooped******\n ");
        Runtime r = Runtime.getRuntime();
        r.addShutdownHook(new Thread(() -> System.out.println("shut down hook task completed..")));
    }
}
